﻿using UnityEngine;
using System.Collections;

public class IntroAnimation : MonoBehaviour {

    public float prePause = 2.0f;
    public float firstPause = 1.0f;
    public float thirdPause = 1.0f;
    public Vector3 tagLightEndingPosition;
    public Vector3 tagLightEndingEuler;
    public Light lightFlash;
    public Light lightLight;
    public Light lightTag;
    public Light lightSelection;
    public Light lightStart;
    public Light lightInstructions;
    public Light lightExit;
    public GameObject goStart;
    public GameObject goInstructions;
    public GameObject goExit;

    public bool introOver = false;
    public bool menuEnabled = false;

    private bool axisInUse = false;
    private int upDown = 0;
    private enum MenuSelection
    {
        START,
        INSTRUCTIONS,
        EXIT
    }
    private MenuSelection currentSelection = MenuSelection.START;
    private MenuSelection previousSelection = MenuSelection.START;
    private SoundScript scriptSound;
    private Animation tagAnimation;
    private Transform transTag;

    void Awake()
    {
        Cursor.visible = false;
    }

	// Use this for initialization
	void Start () 
    {
        tagAnimation = lightTag.GetComponent<Animation>();
        scriptSound = lightTag.GetComponent<SoundScript>();
        transTag = lightTag.transform;
        StartCoroutine("DoTheIntro");
        StartCoroutine(HandleInput());
	}

    void Update()
    {
        if (menuEnabled)
            HandleMenu();
    }

    IEnumerator HandleInput()
    {
        while (!Input.GetButtonDown("Start") && !Input.GetButtonDown("p1Light") && !introOver)
        {
            yield return null;
        }
        if (!introOver)
        {
            StopCoroutine("DoTheIntro");

            if (tagAnimation.isPlaying)
                tagAnimation.Stop();

            lightFlash.enabled = true;
            lightLight.enabled = true;
            lightTag.enabled = true;

            transTag.position = tagLightEndingPosition;
            transTag.rotation = Quaternion.Euler(tagLightEndingEuler);

            scriptSound.mainSource.pitch = 1.0f;
            scriptSound.IntroPlayRight();
        }

        while (!Input.GetButtonDown("Start") && !Input.GetButtonDown("p1Light"))
        {
            yield return null;
        }

        lightSelection.enabled = true;
        lightStart.enabled = true;
        goStart.GetComponent<Renderer>().enabled = true;
        goInstructions.GetComponent<Renderer>().enabled = true;
        goExit.GetComponent<Renderer>().enabled = true;

        menuEnabled = true;

        // Light the start and tutorial menu stuff here
        
    }

    IEnumerator DoTheIntro()
    {
        yield return new WaitForSeconds(prePause);
        scriptSound.IntroPlayFirstFlashlight();
        lightFlash.enabled = true;
        yield return new WaitForSeconds(firstPause);
        scriptSound.IntroPlaySecondFlashlight();
        lightLight.enabled = true;
        yield return new WaitForSeconds(firstPause);
        tagAnimation.Play();
        introOver = true;
        yield return new WaitForSeconds(tagAnimation.GetComponent<Animation>().clip.length + thirdPause);
    }

    void HandleMenu()
    {
        // Handle getting vertical Axis Input
        GetUpDown();

        if (Input.GetButtonDown("Start") || Input.GetButtonDown("p1Light"))
        {
            switch (currentSelection)
            {
                case MenuSelection.START:
                    Application.LoadLevel("setTimeLimit");
                    break;

                case MenuSelection.INSTRUCTIONS:

                    break;

                case MenuSelection.EXIT:
                    Application.Quit();
                    break;
            }
        }
        else if (upDown == -1)
        {
            switch (currentSelection)
            {
                case MenuSelection.START:
                    currentSelection = MenuSelection.INSTRUCTIONS;
                    lightStart.enabled = false;
                    lightInstructions.enabled = true;
                    break;

                case MenuSelection.INSTRUCTIONS:
                    currentSelection = MenuSelection.EXIT;
                    lightInstructions.enabled = false;
                    lightExit.enabled = true;
                    break;

                case MenuSelection.EXIT:
                    currentSelection = MenuSelection.START;
                    lightExit.enabled = false;
                    lightStart.enabled = true;
                    break;
            }
        }

        else if (upDown == 1)
        {
            switch (currentSelection)
            {
                case MenuSelection.START:
                    currentSelection = MenuSelection.EXIT;
                    lightStart.enabled = false;
                    lightExit.enabled = true;
                    break;

                case MenuSelection.INSTRUCTIONS:
                    currentSelection = MenuSelection.START;
                    lightInstructions.enabled = false;
                    lightStart.enabled = true;
                    break;

                case MenuSelection.EXIT:
                    currentSelection = MenuSelection.INSTRUCTIONS;
                    lightExit.enabled = false;
                    lightInstructions.enabled = true;
                    break;
            }
        }
    }

    void GetUpDown()
    {
        if (axisInUse)
        {
            upDown = 0;
        }
        if (!axisInUse && Input.GetAxisRaw("p1Vert") != 0)
        {
            upDown = (int)Input.GetAxisRaw("p1Vert");
            axisInUse = true;
        }
        else if (Input.GetAxisRaw("p1Vert") == 0)
        {
            upDown = 0;
            axisInUse = false;
        }
    }
}
