﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerAltInput : MonoBehaviour {

    public ChainJam.PLAYER playerID;
    public float velocity = 750.0f;
    public float omegaVelocity = 10.0f;
    public float offsetHelper = 1.5f;
    public float rectWidth = 10.0f;
    public float rectHeight = 10.0f;
    public bool useJoy2Key = false;

    public static bool joy2key = false;

    public GameObject p1Arrow;
    public GameObject p2Arrow;
    public GameObject p3Arrow;
    public GameObject p4Arrow;

    private bool helperMenu = false;
    private Transform transPlayer1;
    private Transform transPlayer2;
    private Transform transPlayer3;
    private Transform transPlayer4;
    private Vector3 moveDirection;
    private Quaternion moveRotation;
    private Quaternion helperRotation;


    private CharacterController myCharCont;
    private Transform myTransform;
    private Light myLight;
    private PlayerAltTag scriptAltTag;
    private SoundScript scriptSound;

    void Awake()
    {
        joy2key = useJoy2Key;
    }

	// Use this for initialization
	void Start () 
    {
        transPlayer1 = GameObject.Find("Player1").transform;
        transPlayer2 = GameObject.Find("Player2").transform;
        transPlayer3 = GameObject.Find("Player3").transform;
        transPlayer4 = GameObject.Find("Player4").transform;

        myCharCont = this.GetComponent<CharacterController>();
        myTransform = this.GetComponent<Transform>();
        myLight = this.GetComponentInChildren<Light>();

        helperRotation = Quaternion.Euler(new Vector3(90.0f, 0.0f, 0.0f));
        
        scriptAltTag = this.GetComponentInChildren<PlayerAltTag>();
        scriptSound = this.GetComponent<SoundScript>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        HandleMovement();
        HandleFlashlight();
        HelpMenu();
        HandleTagging();
	}

    void HandleMovement()
    {
        if(GetButton(playerID, ChainJam.BUTTON.B))      // And we're pushing B
            return;                                                     // We don't want to move

        if (GetButton(playerID, ChainJam.BUTTON.UP))
            moveDirection = myTransform.forward;
        else if (GetButton(playerID, ChainJam.BUTTON.DOWN))
            moveDirection = -myTransform.forward;
        else
            moveDirection = Vector3.zero;

        if (GetButton(playerID, ChainJam.BUTTON.RIGHT))
        {
            moveRotation = Quaternion.LookRotation(myTransform.right);
            myTransform.rotation = Quaternion.RotateTowards(myTransform.rotation, moveRotation, omegaVelocity * Time.deltaTime);
        }
        else if (GetButton(playerID, ChainJam.BUTTON.LEFT))
        {
            moveRotation = Quaternion.LookRotation(-myTransform.right);
            myTransform.rotation = Quaternion.RotateTowards(myTransform.rotation, moveRotation, omegaVelocity * Time.deltaTime);
        }

        myCharCont.SimpleMove(moveDirection * velocity * Time.deltaTime);
    }

    void HandleFlashlight()
    {
        if (ChainJam.GetButtonJustPressed(playerID, ChainJam.BUTTON.A))
            if (scriptSound.flashlightOn)
                scriptSound.PlaySound(scriptSound.flashlightOn, playerID, scriptSound.otherSource);
                
        if (GetButton(playerID, ChainJam.BUTTON.A))
        {
            myLight.enabled = true;
        }
        else
            myLight.enabled = false;
    }

    void HelpMenu()
    {
        if (ChainJam.GetButtonJustPressed(playerID, ChainJam.BUTTON.B) && !helperMenu && !(GetButton(playerID, ChainJam.BUTTON.DOWN)) && !(GetButton(playerID, ChainJam.BUTTON.LEFT)) && !(GetButton(playerID, ChainJam.BUTTON.RIGHT)) && !(GetButton(playerID, ChainJam.BUTTON.UP)))   // We just pressed B, the menu isn't up, and we aren't already pressing a direction
        {
            CreateHelperMenu();
        }
        else if (ChainJam.GetButtonJustReleased(playerID, ChainJam.BUTTON.B) && helperMenu)
        {
            RemoveHelperMenu();
        }
    }

    void HandleTagging()
    {
        if (GetButton(playerID, ChainJam.BUTTON.B) && !helperMenu && !(GetButton(playerID, ChainJam.BUTTON.DOWN)) && !(GetButton(playerID, ChainJam.BUTTON.LEFT)) && !(GetButton(playerID, ChainJam.BUTTON.RIGHT)) && !(GetButton(playerID, ChainJam.BUTTON.UP)))
        {
            CreateHelperMenu();
        }
        else if (GetButton(playerID, ChainJam.BUTTON.B) && helperMenu)
        {
            if (GetButton(playerID, ChainJam.BUTTON.UP))     // If we are pressing B + UP 
            {
                if (playerID != ChainJam.PLAYER.PLAYER1)                     // and we are not player1
                {
                    if (CheckTagList(ChainJam.PLAYER.PLAYER1, scriptAltTag.lstPlayer))              // If player1 is in the list of players we have shone our light on
                    {
                        int i = (int)Random.Range(0.0f, 11.0f);
                        scriptSound.PlaySound(scriptSound.aryRight[i], playerID, scriptSound.mainSource);
                        scriptAltTag.Respawn(transPlayer1, ChainJam.PLAYER.PLAYER1);                 // Spawn another copy and destroy the original player1
                        ChainJam.AddPoints(playerID, 1);
                    }
                    else
                    {
                        int i = (int)Random.Range(0.0f, 11.0f);
                        scriptSound.PlaySound(scriptSound.aryWrong[i], playerID, scriptSound.mainSource);
                    }
                }
                else
                {
                    int i = (int)Random.Range(0.0f, 11.0f);
                    scriptSound.PlaySound(scriptSound.aryWrong[i], playerID, scriptSound.mainSource);
                }
                RemoveHelperMenu();
                scriptAltTag.lstPlayer.Clear();                             // Clear the list so we have to find another player before we can tag again OR clear the list because we've misguessed.
            }
            else if (GetButton(playerID, ChainJam.BUTTON.RIGHT))     
            {
                if (playerID != ChainJam.PLAYER.PLAYER2)
                {
                    if (CheckTagList(ChainJam.PLAYER.PLAYER2, scriptAltTag.lstPlayer))
                    {
                        int i = (int)Random.Range(0.0f, 11.0f);
                        scriptSound.PlaySound(scriptSound.aryRight[i], playerID, scriptSound.mainSource);
                        scriptAltTag.Respawn(transPlayer2, ChainJam.PLAYER.PLAYER2);
                        ChainJam.AddPoints(playerID, 1);
                    }
                    else
                    {
                        int i = (int)Random.Range(0.0f, 11.0f);
                        scriptSound.PlaySound(scriptSound.aryWrong[i], playerID, scriptSound.mainSource);
                    }
                }
                else
                {
                    int i = (int)Random.Range(0.0f, 11.0f);
                    scriptSound.PlaySound(scriptSound.aryWrong[i], playerID, scriptSound.mainSource);
                }
                RemoveHelperMenu();
                scriptAltTag.lstPlayer.Clear();
            }
            else if (GetButton(playerID, ChainJam.BUTTON.DOWN))     
            {
                if (playerID != ChainJam.PLAYER.PLAYER3)
                {
                    if (CheckTagList(ChainJam.PLAYER.PLAYER3, scriptAltTag.lstPlayer))
                    {
                        int i = (int)Random.Range(0.0f, 11.0f);
                        scriptSound.PlaySound(scriptSound.aryRight[i], playerID, scriptSound.mainSource);
                        scriptAltTag.Respawn(transPlayer3, ChainJam.PLAYER.PLAYER3);
                        ChainJam.AddPoints(playerID, 1);
                    }
                    else
                    {
                        int i = (int)Random.Range(0.0f, 11.0f);
                        scriptSound.PlaySound(scriptSound.aryWrong[i], playerID, scriptSound.mainSource);
                    }
                }
                else
                {
                    int i = (int)Random.Range(0.0f, 11.0f);
                    scriptSound.PlaySound(scriptSound.aryWrong[i], playerID, scriptSound.mainSource);
                }
                RemoveHelperMenu();
                scriptAltTag.lstPlayer.Clear();
            }
            else if (GetButton(playerID, ChainJam.BUTTON.LEFT))     
            {
                if (playerID != ChainJam.PLAYER.PLAYER4)
                {
                    if (CheckTagList(ChainJam.PLAYER.PLAYER4, scriptAltTag.lstPlayer))
                    {
                        int i = (int)Random.Range(0.0f, 11.0f);
                        scriptSound.PlaySound(scriptSound.aryRight[i], playerID, scriptSound.mainSource);
                        scriptAltTag.Respawn(transPlayer4, ChainJam.PLAYER.PLAYER4);
                        ChainJam.AddPoints(playerID, 1);
                    }
                    else
                    {
                        int i = (int)Random.Range(0.0f, 11.0f);
                        scriptSound.PlaySound(scriptSound.aryWrong[i], playerID, scriptSound.mainSource);
                    }
                }
                else
                {
                    int i = (int)Random.Range(0.0f, 11.0f);
                    scriptSound.PlaySound(scriptSound.aryWrong[i], playerID, scriptSound.mainSource);
                }
                RemoveHelperMenu();
                scriptAltTag.lstPlayer.Clear();
            }
        }
    }

    public bool CheckTagList(ChainJam.PLAYER parPlayer, List<ChainJam.PLAYER> parList)
    {
        foreach (ChainJam.PLAYER p in parList)
        {
            if (p == parPlayer)
                return true;
        }
        return false;
    }

    void CreateHelperMenu()
    {
        GameObject p1Clone = GameObject.Instantiate(p1Arrow, myTransform.position + new Vector3(0.0f, 0.0f, offsetHelper), helperRotation) as GameObject;
        GameObject p2Clone = GameObject.Instantiate(p2Arrow, myTransform.position + new Vector3(offsetHelper, 0.0f, 0.0f), helperRotation) as GameObject;
        GameObject p3Clone = GameObject.Instantiate(p3Arrow, myTransform.position + new Vector3(0.0f, 0.0f, -offsetHelper), helperRotation) as GameObject;
        GameObject p4Clone = GameObject.Instantiate(p4Arrow, myTransform.position + new Vector3(-offsetHelper, 0.0f, 0.0f), helperRotation) as GameObject;
        scriptSound.PlaySound(scriptSound.menuOn, playerID, scriptSound.otherSource);
        helperMenu = true;
    }

    void RemoveHelperMenu()
    {
        GameObject[] goAry = GameObject.FindGameObjectsWithTag("HelperArrow");
        if (goAry != null)
        {
            foreach (GameObject go in goAry)
            {
                GameObject.Destroy(go);
            }
            helperMenu = false;
        }
    }

    bool GetButton(ChainJam.PLAYER player, ChainJam.BUTTON button)
    {
        if (!joy2key)
        {
            switch (button)
            {
                case ChainJam.BUTTON.A:
                case ChainJam.BUTTON.B:

                    return ChainJam.GetButtonPressed(player, button);
                    

                case ChainJam.BUTTON.UP:
                case ChainJam.BUTTON.DOWN:
                case ChainJam.BUTTON.RIGHT:
                case ChainJam.BUTTON.LEFT:

                    return ChainJam.GetAxis(player, button);
            }
            // This should never happen
            return false;
        }
        else
        {
            return ChainJam.GetButtonPressed(player, button);
        }
    }

}
