﻿using UnityEngine;
using System.Collections;

public class SoundScript : MonoBehaviour {

    public AudioClip flashlightOn;
    public AudioClip menuOn;
    public AudioClip[] arySight = new AudioClip[12];
    public AudioClip[] aryRight = new AudioClip[12];
    public AudioClip[] aryWrong = new AudioClip[12];


    public AudioSource mainSource;
    public AudioSource otherSource;

	// Use this for initialization
	void Start () 
    {
        mainSource = this.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void PlaySound(AudioClip clip, ChainJam.PLAYER id, AudioSource parSource)
    {
        parSource.clip = clip;
            switch (id)
            {
                case ChainJam.PLAYER.PLAYER1:
                    parSource.pitch = 1;
                    break;
                case ChainJam.PLAYER.PLAYER2:
                    parSource.pitch = 1.5f;
                    break;
                case ChainJam.PLAYER.PLAYER3:
                    parSource.pitch = 0.5f;
                    break;
                case ChainJam.PLAYER.PLAYER4:
                    parSource.pitch = 0.75f;
                    break;
            }
        parSource.Play();
    }

    public void IntroPlayFirstFlashlight()
    {
        mainSource.clip = flashlightOn;
        mainSource.pitch = 0.75f;
        mainSource.Play();
    }

    public void IntroPlaySecondFlashlight()
    {
        mainSource.clip = flashlightOn;
        mainSource.pitch = 1.25f;
        mainSource.Play();
    }

    public void IntroPlayThirdFlashlight()
    {
        mainSource.clip = flashlightOn;
        mainSource.pitch = 1.0f;
        mainSource.Play();
    }

    public void IntroPlaySight()
    {
        mainSource.clip = arySight[(int)Random.Range(0.0f, 11.0f)];
        mainSource.Play();
    }

    public void IntroPlayRight()
    {
        mainSource.clip = aryRight[(int)Random.Range(0.0f, 11.0f)];
        mainSource.Play();
    }
}
