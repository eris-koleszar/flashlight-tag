﻿using UnityEngine;
using System.Collections;

public class MusicScript : MonoBehaviour {

    public AudioClip[] aryMusic = new AudioClip[12];

    private int lastMotif;
    private int nextMotif = 0;
    private int nextGap = 0;
    private AudioSource mySource;

	// Use this for initialization
	void Start () 
    {
        mySource = this.GetComponent<AudioSource>();
        StartCoroutine(MakeMusic());
	}

    IEnumerator MakeMusic()
    {
        while (true)
        {
            while (nextMotif == lastMotif)      // Don't play the same motif twice in a row.
            {
                nextMotif = (int)Random.Range(0.0f, 11.0f);
            }
            nextGap = (int)Random.Range(2.0f, 6.0f);
            mySource.clip = aryMusic[nextMotif];
            mySource.Play();
            yield return new WaitForSeconds(nextGap + aryMusic[nextMotif].length);
            lastMotif = nextMotif;
        }
    }

    
}
