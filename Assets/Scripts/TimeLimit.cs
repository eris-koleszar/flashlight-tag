﻿using UnityEngine;
using System.Collections;

public class TimeLimit : MonoBehaviour {

    public Font timeFont;
    public Color timeColor;

    public int timeLimit = 1;
    public float rectWidth = 300.0f;
    public float rectHeight = 50.0f;
    public float timerRectW = 50.0f;
    public float timerRectH = 30.0f;
    public bool timeIsSet = false;
    public bool timesUp = false;

    private bool axisInUse = false;
    private int upDown = 0;
    private bool showStart = false;
    private GUIStyle myStyle = new GUIStyle();
    private float timecounter;
    private int minutes;
    private int seconds;
    private string gameWinner;
    private AudioSource mySource;

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }
	// Use this for initialization
	void Start () 
    {
        mySource = this.GetComponent<AudioSource>();
        myStyle.font = timeFont;
        myStyle.normal.textColor = timeColor;
        myStyle.alignment = TextAnchor.MiddleCenter;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (!timeIsSet && Application.loadedLevel == 1)         // If we haven't set the time yet and we're not on the intro screen
            TimeInput();                                        // Let us set time
        else if (!timesUp && Application.loadedLevel == 2)      // If time hasn't run out yet and we're playing the game
            Countdown();                                        // Countdown the clock
        else if(Application.loadedLevel == 2)                   // Else, if we're playing the game, the time is up and we should reset
            ResetTheGame();
	}

    void OnGUI()
    {
        if (!timeIsSet && Application.loadedLevel == 1)
        {
            myStyle.fontSize = 32;
            GUI.Label(new Rect((Screen.width / 2) - (rectWidth / 2), (Screen.height / 2) - (rectHeight / 2), rectWidth, rectHeight), "Set Time Limit:  " + timeLimit + " minute(s)", myStyle);
            //print("Should be setting the time limit now.");
        }
        else if (timesUp)
        {
            myStyle.fontSize = 128;
            GUI.Label(new Rect((Screen.width / 2) - (rectWidth / 2), (Screen.height / 2) - (rectHeight / 2), rectWidth, rectHeight), "Winner: " + gameWinner + "!", myStyle);
            if (showStart)
            {
                myStyle.fontSize = 64;
                GUI.Label(new Rect((Screen.width / 2) - (rectWidth / 2), (Screen.height * 3 / 4) - (rectHeight / 2), rectWidth, rectHeight), "Press Start", myStyle);
            }
            //print("The game is over; should be displaying the winner.");
        }
        else if (Application.loadedLevel == 2 && timeIsSet)
        {
            myStyle.fontSize = 32;
            GUI.Label(new Rect((Screen.width / 2) - (timerRectW / 2), 0.0f, timerRectW, timerRectH), minutes + ":" + seconds.ToString("00"), myStyle);
            //print("Should be counting down now.");
        }
    }

    void TimeInput()
    {
        GetUpDown();

        if(upDown == 1 && timeLimit < 99)
        {
            timeLimit++;
        }
        else if (upDown == -1 && timeLimit > 1)
        {
            timeLimit--;
        }
        else if (Input.GetButtonDown("p1Light"))
        {
            timeIsSet = true;
            timecounter = timeLimit;
            Application.LoadLevel("StandAlone");
        }
    }

    void GetUpDown()
    {
        if (axisInUse)
        {
            upDown = 0;
        }
        if (!axisInUse && Input.GetAxisRaw("p1Vert") != 0)
        {
            upDown = (int)Input.GetAxisRaw("p1Vert");
            axisInUse = true;
        }
        else if (Input.GetAxisRaw("p1Vert") == 0)
        {
            upDown = 0;
            axisInUse = false;
        }
    }

    void Countdown()
    {
        if (timecounter > 0.0f)
        {
            timecounter -= Time.deltaTime/60;
            minutes = Mathf.FloorToInt(timecounter);
            seconds = Mathf.FloorToInt((timecounter - minutes) * 60.0f);
        }
        else
        {
            timesUp = true;
            DetermineWinner();
            StartCoroutine(PressStart());
            StopTheMusic();
            mySource.Play();
        }
    }

    void DetermineWinner()
    {
        int[] playerPoints = new int[4];

        playerPoints[0] = ChainJam.GetPoints(ChainJam.PLAYER.PLAYER1);
        playerPoints[1] = ChainJam.GetPoints(ChainJam.PLAYER.PLAYER2);
        playerPoints[2] = ChainJam.GetPoints(ChainJam.PLAYER.PLAYER3);
        playerPoints[3] = ChainJam.GetPoints(ChainJam.PLAYER.PLAYER4);

        int highestScore = 0;
        int winner = 9;

        for (int i = 0; i < playerPoints.Length; i++)
        {
            if (playerPoints[i] > highestScore)
            {
                highestScore = playerPoints[i];
                winner = i;
            }
            else if (playerPoints[i] == highestScore && winner != 9)
                winner = 5;
        }
        if (winner == 0)
            gameWinner = "Player 1";
        else if (winner == 1)
            gameWinner = "Player 2";
        else if (winner == 2)
            gameWinner = "Player 3";
        else if (winner == 3)
            gameWinner = "Player 4";
        else if (winner == 9 || winner == 5)
            gameWinner = "Tie";

    }

    void ResetTheGame()
    {
        if (Input.GetButtonDown("Start"))
        {
            timeIsSet = false;
            timesUp = false;
            GameObject chainController = GameObject.Find("_ChainJam");
            Destroy(chainController);
            ChainJam.ResetScores();
            Application.LoadLevel("introMenu");
            Destroy(this.gameObject);
        }
    }

    IEnumerator PressStart()
    {
        yield return new WaitForSeconds(2.0f);
        showStart = true;
    }

    void StopTheMusic()
    {
        GameObject goMusicHandler = GameObject.Find("MusicHandler");
        MusicScript scriptMusic = goMusicHandler.GetComponent<MusicScript>();

        scriptMusic.StopAllCoroutines();

    }
}
