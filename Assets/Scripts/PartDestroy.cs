﻿using UnityEngine;
using System.Collections;

public class PartDestroy : MonoBehaviour {

	// Use this for initialization
	void Start () 
    {
        Invoke("ClearTheAir", 0.6f);
	}

    void ClearTheAir()
    {
        GameObject.Destroy(this.gameObject);
    }
}
