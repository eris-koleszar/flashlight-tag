﻿using UnityEngine;
using System.Collections;

public class EndGame : MonoBehaviour {

	
	// Update is called once per frame
	void Update () 
    {
        if (ChainJam.GetTotalPoints() == 10)
            ChainJam.GameEnd();
	}
}
