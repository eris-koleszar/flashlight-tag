﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerAltTag : MonoBehaviour {

    public float spawnWidth = 42.0f;
    public float spawnHeight = 22.0f;
    public float testRadius = 5.0f;
    public ParticleSystem p1Part;
    public ParticleSystem p2Part;
    public ParticleSystem p3Part;
    public ParticleSystem p4Part;
    public List<ChainJam.PLAYER> lstPlayer = new List<ChainJam.PLAYER>();

    private SoundScript scriptSound;
    private PlayerAltInput scriptInput;
    private PlayerAltTag[] playerTags = new PlayerAltTag[4];
    private GameObject[] goPlayers;
    private Transform myTransform;
    private Light myLight;

    // Use this for initialization
    void Start()
    {
        scriptInput = this.transform.parent.GetComponent<PlayerAltInput>();
        myTransform = this.transform;
        scriptSound = myTransform.parent.GetComponent<SoundScript>();
        myLight = myTransform.parent.GetComponentInChildren<Light>();

        goPlayers = GameObject.FindGameObjectsWithTag("Player");
        for (int i = 0; i < playerTags.Length; i++)
        {
            playerTags[i] = goPlayers[i].GetComponentInChildren<PlayerAltTag>();
        }
        //InvokeRepeating("DisplayList", 0.0f, 0.5f);
    }

    //void OnTriggerEnter(Collider other)
    //{
    //    if (other.name != myTransform.parent.name && other.tag == "Player")
    //    {
    //        StartCoroutine("AddToList", other);
    //    }
    //}

    //void OnTriggerExit(Collider other)
    //{
    //    if (other.name != myTransform.parent.name && other.tag == "Player")
    //    {
    //        StopCoroutine("AddToList");
    //    }
    //}


    //IEnumerator AddToList(Collider parOther)
    //{
    //    Vector3 direction = (parOther.transform.position - myTransform.position);
    //    float angle = Vector3.Angle(direction, myTransform.forward);
    //    while (angle > myLight.spotAngle * 0.5 || !myLight.enabled)
    //    {
    //        yield return null;
    //    }
    //    PlayerAltInput scriptOthersInput = parOther.GetComponent<PlayerAltInput>();
    //    lstPlayer.Add(scriptOthersInput.playerID);                                                            // Add the player we are hitting with the light to the list of players we can tag
    //}

    void OnTriggerStay(Collider other)
    {
        if (other.name != myTransform.parent.name && other.tag == "Player")
        {
            if (myLight.enabled)
            {
                Vector3 direction = (other.transform.position - myTransform.position);
                float angle = Vector3.Angle(direction, myTransform.forward);
                if (angle <= myLight.spotAngle * 0.5)
                {
                    PlayerAltInput scriptOthersInput = other.GetComponent<PlayerAltInput>();
                    if (!scriptOthersInput.CheckTagList(scriptOthersInput.playerID, lstPlayer))             // If the playerID is not already in the list
                    {
                        int i = (int)Random.Range(0.0f, 11.0f);
                        scriptSound.PlaySound(scriptSound.arySight[i], scriptInput.playerID, scriptSound.mainSource);   // Play the "Sighted" sound
                        lstPlayer.Add(scriptOthersInput.playerID);                                          // Add it to the list
                    }
                }
            }
        }
    }

    public void Respawn(Transform parTransPlayer, ChainJam.PLAYER pID)
    {
        switch (pID)
        {
            case ChainJam.PLAYER.PLAYER1:
                GameObject p1Clone = GameObject.Instantiate(p1Part.gameObject, parTransPlayer.position, Quaternion.identity) as GameObject;
                foreach(PlayerAltTag pat in playerTags)
                {
                    if (scriptInput.CheckTagList(ChainJam.PLAYER.PLAYER1, pat.lstPlayer))
                        pat.lstPlayer.Remove(ChainJam.PLAYER.PLAYER1);
                }
                break;
            case ChainJam.PLAYER.PLAYER2:
                GameObject p2Clone = GameObject.Instantiate(p2Part.gameObject, parTransPlayer.position, Quaternion.identity) as GameObject;
                foreach (PlayerAltTag pat in playerTags)
                {
                    if (scriptInput.CheckTagList(ChainJam.PLAYER.PLAYER2, pat.lstPlayer))
                        pat.lstPlayer.Remove(ChainJam.PLAYER.PLAYER2);
                }
                break;
            case ChainJam.PLAYER.PLAYER3:
                GameObject p3Clone = GameObject.Instantiate(p3Part.gameObject, parTransPlayer.position, Quaternion.identity) as GameObject;
                foreach (PlayerAltTag pat in playerTags)
                {
                    if (scriptInput.CheckTagList(ChainJam.PLAYER.PLAYER3, pat.lstPlayer))
                        pat.lstPlayer.Remove(ChainJam.PLAYER.PLAYER3);
                }
                break;
            case ChainJam.PLAYER.PLAYER4:
                GameObject p4Clone = GameObject.Instantiate(p4Part.gameObject, parTransPlayer.position, Quaternion.identity) as GameObject;
                foreach (PlayerAltTag pat in playerTags)
                {
                    if (scriptInput.CheckTagList(ChainJam.PLAYER.PLAYER4, pat.lstPlayer))
                        pat.lstPlayer.Remove(ChainJam.PLAYER.PLAYER4);
                }
                break;
        }

        Quaternion spawnRotation = Quaternion.Euler(0.0f, Random.Range(0, 359), 0.0f);                                      // Create a random rotation
        Vector3 spawnPoint = new Vector3(Random.Range(-spawnWidth, spawnWidth), 1.1f, Random.Range(-spawnHeight, spawnHeight));

        RaycastHit hit;
        Physics.SphereCast(spawnPoint + new Vector3(0.0f, 10.0f, 0.0f), testRadius, Vector3.down, out hit, 10.0f);
        while (hit.transform.tag == "Player")                                                                                       // As long as we're too close to other players
        {
            spawnPoint = new Vector3(Random.Range(-spawnWidth, spawnWidth), 1.1f, Random.Range(-spawnHeight, spawnHeight));         // Find a new position
            Physics.SphereCast(spawnPoint + new Vector3(0.0f, 10.0f, 0.0f), testRadius, Vector3.down, out hit, 10.0f);              // Check again
        }
        PlayerAltTag scriptOtherTag = parTransPlayer.GetComponentInChildren<PlayerAltTag>();
        scriptOtherTag.lstPlayer.Clear();
        parTransPlayer.position = spawnPoint;                   // Teleport the player to the new position
        parTransPlayer.rotation = spawnRotation;                // Give the player a new, random rotation


    }

    void DisplayList()
    {
        foreach (ChainJam.PLAYER p in lstPlayer)
        {
            print(p);
        }
    }
}
