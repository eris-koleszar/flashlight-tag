﻿using UnityEngine;
using System.Collections;

public class GUIPoints : MonoBehaviour {

    public Color player1;
    public Color player2;
    public Color player3;
    public Color player4;
    public Font scoreFont;
    public float divideBy = 10.0f;
    public float timesBy = 9.0f;
    public float boxWidth = 40.0f;
    public float boxHeight = 20.0f;

    private float p1x;
    private float p1y;
    private float p2x;
    private float p2y;
    private float p3x;
    private float p3y;
    private float p4x;
    private float p4y;


    private GUIStyle style1 = new GUIStyle();
    private GUIStyle style2 = new GUIStyle();
    private GUIStyle style3 = new GUIStyle();
    private GUIStyle style4 = new GUIStyle();


    void Awake()
    {
        Cursor.visible = false;
    }

	// Use this for initialization
	void Start () 
    {
        p1x = Screen.width / divideBy;
        p1y = Screen.width / divideBy;
        p2x = Screen.width / divideBy * timesBy;
        p2y = Screen.width / divideBy;
        p3x = Screen.width / divideBy;
        p3y = Screen.height / divideBy * timesBy;
        p4x = Screen.width / divideBy * timesBy;
        p4y = Screen.height / divideBy * timesBy;

        print("P1 x: " + p1x + " P1 y: " + p1y + "  P2 x: " + p2x + " P2 y: " + p2y + "  P3 x: " + p3x + " P3 y: " + p3y + "  P4 x: " + p4x + " P4 y: " + p4y);

        if (scoreFont)
        {
            style1.font = scoreFont;
            style2.font = scoreFont;
            style3.font = scoreFont;
            style4.font = scoreFont;
        }
        else
            Debug.Log("Please assign the font in the GUI script.");

        style1.normal.textColor = player1;
        style2.normal.textColor = player2;
        style3.normal.textColor = player3;
        style4.normal.textColor = player4;

	}

    void OnGUI()
    {
        GUI.Label(new Rect(p1x, p1y, boxWidth, boxHeight), ChainJam.GetPoints(ChainJam.PLAYER.PLAYER1).ToString(), style1);
        GUI.Label(new Rect(p2x, p2y, boxWidth, boxHeight), ChainJam.GetPoints(ChainJam.PLAYER.PLAYER2).ToString(), style2);
        GUI.Label(new Rect(p3x, p3y, boxWidth, boxHeight), ChainJam.GetPoints(ChainJam.PLAYER.PLAYER3).ToString(), style3);
        GUI.Label(new Rect(p4x, p4y, boxWidth, boxHeight), ChainJam.GetPoints(ChainJam.PLAYER.PLAYER4).ToString(), style4);
    }
}
